// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

/* Load Package */
import VueRouter from 'vue-router';
import axios from 'axios';

/* Load Components */
import Home from './components/home.vue';
import Quotes from './components/quotes.vue';
import NewQuote from './components/new-quote.vue';

import Users from './components/users.vue';

Vue.use(VueRouter);

Vue.prototype.$axios = axios;
Vue.prototype.$backendUrl = 'http://backend-machine.local';

const routes = [
  {
    path : '/', component: Home
  },
  {
    path : '/quotes', component: Quotes
  },
  {
    path : '/users', component: Users
  },
  {
    path : '/new-quote', component: NewQuote
  },
];

const router = new VueRouter({
  routes : routes,
  mode : 'history'
});

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router : router,
  components: { App },
  template: '<App/>'
})
